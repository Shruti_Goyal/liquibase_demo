--liquibase formatted sql

--changeset Script05:0005
create table test5 (
id int primary key,
name varchar(255)
);

--rollback DROP TABLE test5